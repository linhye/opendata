<?php
    include("liens.php");
    $donnees = new donnees;
    $donnees->getLink();
    
    $url2 = "";
    $type_formation = "";
    $domaine = "";
    $diplome= "";
    $reg = "";
    $departement= "";
    $etablissement= "";

    $results4 = "";

    if (isset($_POST['formation']) && $_POST['formation'] != "") {
        $type_formation = "&refine.niveau_lib=".$_POST['formation'];
    }

    if (isset($_POST['domaine']) && $_POST['domaine'] != "") {
        $domaine = "&refine.sect_disciplinaire_lib=".$_POST['domaine'];
    }

    if (isset($_POST['choix_diplome']) && $_POST['choix_diplome'] != "") {
        $diplome = "&refine.diplome_rgp=".$_POST['choix_diplome'];
    }

    if (isset($_POST['choix_region']) && $_POST['choix_region'] != "") {
        $reg = "&refine.reg_ins_lib=".$_POST['choix_region'];
    }

    if (isset($_POST['departement']) && $_POST['departement'] != "") {
        $departement = "&refine.dep_etab_lib=".$_POST['departement'];
    }

    if (isset($_POST['etablissement']) && $_POST['etablissement'] != "") {
        $etablissement = "refine.etablissement=".$_POST["uai"];
    }

    if (isset($_POST['$type_formation']) || isset($_POST['domaine']) || isset($_POST['diplome']) || isset($_POST['reg'])|| isset($_POST['departement'])) {
        $url2 = $donnees->getLien().$donnees->getLignes()."&sort=-rentree_lib&facet=etablissement_lib&facet=diplome_rgp&facet=sect_disciplinaire_lib&facet=niveau_lib&facet=etablissement&refine.rentree_lib=2017-18".$type_formation.$domaine.$diplome.$reg.$departement.$donnees->getAPI();
    }

    if($url2 != "") {
        $contents = file_get_contents($url2);
        $results4 = json_decode($contents,true);
    }
?>