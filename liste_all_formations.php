<?php 
if(!isset($_GET['etb'])) {
    header("Location: listes.php");
}
$id = $_GET['etb'];
include("liens2.php");

$url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=9999&refine.rentree_lib=2017-18&refine.etablissement=".$id."&apikey=bd53055fa09316bdec85942437a5b3c5d621019e4f25acd60606af07";
$json = file_get_contents($url);
$data4 = json_decode($json,true);
include("header.php");
?>

<body>
<div class = "header">
    <h1>Trouver ma formation</h1>
</div>
<div class = "navigation">
    <nav>
        <a href ="index.php">Recherche d'établissement</a>
        <a href ="listes.php">Recherche de formations</a>
    </nav>
</div>
<br>
    
<div class = "liste_ecoles_etab">

             <!--resultat des recherches-->
             <?php 

             echo "<h1>".$data4["records"][0]["fields"]["etablissement_lib"]."</h1>"; ?>
            <table id="table_id">
                <thead>
                    <tr>
                        <td>Académie</td>
                        <td>Discipline</td>
                        <td>En savoir plus</td>
                    </tr>
                </thead>
                <tbody> 
                    <?php
                        foreach ($data4["records"] as $res) {
                            echo "<tr>";
                            echo "<td>";
                            echo $res["fields"]["aca_etab_lib"];
                            echo "</td>";
                            echo "<td>";
                            echo $res["fields"]["sect_disciplinaire_lib"];
                            echo "</td>";
                            echo "<td>";
                            echo "<a href='fiches_formations.php?id=".$res["recordid"]."'>Plus d'informations</a>";
                            echo "</td>"; 
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
          </div>

        </div>

        <?php include("footer.php"); ?>
</body>
