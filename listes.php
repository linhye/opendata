<?php
	include("header.php");	
	include("liens2.php");
	
?>
	<body>

		<div class = "header">
			<h1>Trouver ma formation</h1>
		</div>
		<div class = "navigation">
			<nav>
				<a href ="index.php">Recherche d'établissement</a>
				<a href ="listes.php">Recherche de formations</a>
			</nav>
		</div>
		<br>

        <!--BARRE DE RECHERCHE : FILTRE-->
		<div class= "first_bloc">

			<div class= "liste_rechercher">
				<form method="POST" action = "listes.php">
					<h4>Année de formation :</h4>
					<select name="formation">
						<option value="">Année de formation</option>
							<?php
								$data1 = $donnees->getFormation($donnees->getLien(),$donnees->getAPI(),$donnees->getLignes());
								$liste = array(); 
                                foreach($data1["facet_groups"][0]["facets"] as $l1){
									array_push($liste, '<option value ="'.$l1["name"].'">'.$l1["name"].'</option>');
								}
								sort($liste);
								foreach($liste as $l){
									echo $l;
								}
							?>
					</select><br><br>

					<h4>Domaine :</h4>
						<select name="domaine">
							<option value="">Domaine</option>
							<?php
							$data2 = $donnees->getDomaine($donnees->getLien(),$donnees->getAPI(),$donnees->getLignes());
							$liste = array(); 	
							foreach($data2["facet_groups"][0]["facets"] as $l2){
									array_push($liste,'<option value ="'.$l2["name"].'">'.$l2["name"].'</option>') ;
									
								}
								sort($liste);
								foreach($liste as $l){
									echo $l;
								}
							?>
						</select><br><br>

					<h4>Diplome</h4>
					<select name="choix_diplome">
						<option value=""> Sélectionnez un type de diplôme</option>
					   <?php
					   $data3 = $donnees->getDiplome($donnees->getLien(),$donnees->getAPI(),$donnees->getLignes());
					   $liste = array(); 
                            foreach($data3["facet_groups"][0]["facets"] as $l3){
                                array_push($liste, '<option value ="'.$l3["name"].'">'.$l3["name"].'</option>') ;
							}
							sort($liste);
								foreach($liste as $l){
									echo $l;
								}
						?>
					</select>

					<h4>Région</h4>
					<select name="choix_region">
						<option value=""> Sélectionnez une région </option>
						<?php
						$data4 = $donnees->getRegion($donnees->getLien(),$donnees->getAPI(),$donnees->getLignes());
						$liste = array(); 
                            foreach($data4["facet_groups"][0]["facets"] as $l4){
                                array_push($liste, '<option value ="'.$l4["name"].'">'.$l4["name"].'</option>' ) ;
							}
							sort($liste);
								foreach($liste as $l){
									echo $l;
								}
						?>
					</select>

					<h4>Départements :</h4>
					<select name="departement">
						<option value="">Départements</option>
						<?php
						$data5 = $donnees->getDepartement($donnees->getLien(),$donnees->getAPI(),$donnees->getLignes() );
						$liste = array(); 
                            foreach($data5["facet_groups"][0]["facets"] as $l5){
                               array_push($liste, '<option value ="'.$l5["name"].'">'.$l5["name"].'</option>') ;
							}
							sort($liste);
								foreach($liste as $l){
									echo $l;
								}
                        ?>
					</select><br><br><br>
					<input name='recherche' type="submit" value="Rechercher"/>
				</form>
			</div>

            <!--MAP-->

		<div id="mapid">   
        </div>
		<?php
		if(!isset($results4)){
			echo "<h2>/!\ Erreur de connexion à l'API /!\ </h2>";
		} else {
			echo "<h2>Nb de résultats (".$results4["nhits"].")</h2>";
		}
		?>
		<div class = "liste_ecoles">

             <!--resultat des recherches-->
			<table id="table_id">
                <thead>
                    <tr>
    					<td>Nom de l'établissement</td>
    					<td>Académie</td>
    					<td>Discipline</td>
                        <td>En savoir plus</td>
					</tr>
                </thead>


                <tbody> 
					<?php
						if (isset($_POST['recherche'])) {
							foreach ($results4["records"] as $res) {
								echo "<tr>";
								echo "<td>";
								echo $res["fields"]["etablissement_lib"];
								echo "</td>";
								echo "<td>";
								echo $res["fields"]["aca_etab_lib"];
								echo "</td>";
								echo "<td>";
								echo $res["fields"]["sect_disciplinaire_lib"];
								echo "</td>";
								echo "<td>";
								echo "<a href='fiches_formations.php?id=".$res["recordid"]."'>Plus d'informations</a>";
								echo "</td>"; 
								echo "</tr>";
							}
							}
						?>
                </tbody>
			</table>
		  </div>

		</div>
		<?php 
		include("carte.php");
		include("footer.php");
	?>
	</body>

</html>