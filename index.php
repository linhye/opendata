<?php
    include("header.php");    
    include("liens2.php");
    $api_etab = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&rows=322&apikey=bd53055fa09316bdec85942437a5b3c5d621019e4f25acd60606af07"; 
    $json1 = file_get_contents($api_etab);
    $data1 = json_decode($json1,true); 
?>
    <body>
        <div class = "header">
            <h1>Trouver ma formation</h1>
        </div>
        <div class = "navigation">
            <nav>
                <a href ="index.php">Recherche d'établissement</a>
                <a href ="listes.php">Recherche de formation</a>
            </nav>
        </div>
        <div class = "Rechercher">
        <br>
        <form id="recherche_etab" action="liste_all_formations.php" method="get">
            <h4>Rechercher un établissement : </h4>
            <select name="etb">
                <option value="">Etablissement</option>
                <?php   
                $liste = array(); 
                foreach($data1["records"] as $l2){
                    $nom = $l2["fields"]["uo_lib"]; 
                    $uai = $l2["fields"]["uai"];
                    array_push($liste,array($nom,"<option value ='".$uai."'>".$nom."</option>"));
                      
                }
                sort($liste);
                foreach($liste as $l){
                    echo $l[1];
                }
                ?>
            </select><br><br>
            <input  type="submit" value="Rechercher"/>
        </form>
            
            </div>

<?php include("footer.php"); ?>
        </body>
</html>