<?php 
class donnees{
    private $lien;
    private $api;
    private $lignes;
    private $id;
    

    public function getLink(){
        $this->lien = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics";
        $this->api="&apikey=bd53055fa09316bdec85942437a5b3c5d621019e4f25acd60606af07";
        $this->lignes="&rows=100";
    }

    public function getLien(){
        return $this->lien;
    }

    public function getAPI(){
        return $this->api;
    }

    public function getLignes(){
        return $this->lignes;
    }

    public function getId(){
        return $this->id;
    }

    public function getFormation($lien, $api, $lignes){
        $lien = $lien.$lignes."&facet=niveau_lib&refine.rentree_lib=2017-18".$api;
        $json = file_get_contents($lien);
        $data1 = json_decode($json,true);
        return $data1;
    }

    public function getDomaine($lien, $api, $lignes){
        $lien = $lien.$lignes."&facet=sect_disciplinaire_lib&refine.rentree_lib=2017-18".$api;
        $json = file_get_contents($lien);
        $data2 = json_decode($json,true);
        return $data2;
    }

    public function getDiplome($lien, $api, $lignes){
        $lien = $lien.$lignes."&facet=diplome_rgp&refine.rentree_lib=2017-18".$api;
        $json = file_get_contents($lien);
        $data3 = json_decode($json,true);
        return $data3;
    }

    public function getRegion($lien, $api, $lignes){
        $lien = $lien.$lignes."&facet=reg_ins_lib&refine.rentree_lib=2017-18".$api;
        $json = file_get_contents($lien);
        $data4 = json_decode($json,true);
        return $data4;
    }

    public function getDepartement($lien, $api, $lignes){
        $lien = $lien.$lignes."&facet=dep_etab_lib&refine.rentree_lib=2017-18".$api;
        $json = file_get_contents($lien);
        $data5 = json_decode($json,true);
        return $data5;
    }

    public function getRecordFormation($lien, $lignes, $id){
        $lien = $lien.$lignes."&facet=dep_etab_lib&refine.rentree_lib=2017-18&refine.etablissement=".$id;
        $json = file_get_contents($lien);
        $data5 = json_decode($json,true);
        return $data5;
    }

    public function getNomEtab($lien, $api, $lignes){
        $lien = $lien.$lignes."&facet=etablissement_lib&refine.rentree_lib=2017-18".$api;
        $json = file_get_contents($lien);
        $data4 = json_decode($json,true);
        return $data4;
    }


}

?>