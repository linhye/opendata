
<script>
    var mymap = L.map('mapid').setView([48.862725,2.287592], 5);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoibGluaHllIiwiYSI6ImNrMzYyZjI1aTBoc28zb212eDFnc3Q2bGgifQ.FL9ECU7HOr-oobG6Kv6dSQ'
    }).addTo(mymap);
    
    <?php
        foreach ($results4["facet_groups"][3]["facets"] as $res) {
            $url3 = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&facet=uai&facet=type_d_etablissement&refine.uai=".$res["name"];
            $json = file_get_contents($url3);
            $data6 = json_decode($json,true);

            if(isset($data6["records"][0]["fields"]["uo_lib"])) {
                $x = $data6["records"][0]["fields"]["coordonnees"][0];
                $y = $data6["records"][0]["fields"]["coordonnees"][1];
                echo 'L.marker(['.$x.', '.$y.']).addTo(mymap).bindPopup("'.$data6["records"][0]["fields"]["uo_lib"].'</b>");';
            }
        }
    ?>

    
</script>