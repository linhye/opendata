<?php
    if(!isset($_GET['id'])) {
        header("Location: listes.php");
    }
    $id = $_GET['id'];
    include("liens2.php");

    $lien = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=100&refine.rentree_lib=2017-18&refine.recordid=".$id."&apikey=bd53055fa09316bdec85942437a5b3c5d621019e4f25acd60606af07";
    $json = file_get_contents($lien);
    $data = json_decode($json,true);
    $data = $data["records"][0]["fields"];
    
    $id_etab = $data["etablissement"];
    $api_etab = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&sort=uo_lib&refine.uai=".$id_etab."&apikey=bd53055fa09316bdec85942437a5b3c5d621019e4f25acd60606af07"; 
    $json1 = file_get_contents($api_etab);
    $data1 = json_decode($json1,true);
    $data1 = $data1["records"][0]["fields"];

    include("header.php");	
    
?>


    


<body>

		<div class = "header">
			<h1>Trouver ma formation</h1>
		</div>
		<div class = "navigation">
			<nav>
				<a href ="index.php">Recherche d'établissement</a>
				<a href ="listes.php">Recherche de formations</a>
			</nav>
		</div>
        <br>
        <div id="mapid" class = "mapid_formation">

        </div>
        <script>
            var mymap = L.map('mapid').setView([48.862725,2.287592], 5);
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoibGluaHllIiwiYSI6ImNrMzYyZjI1aTBoc28zb212eDFnc3Q2bGgifQ.FL9ECU7HOr-oobG6Kv6dSQ'
            }).addTo(mymap);
            
            <?php
            if(isset($data1["uo_lib"])) {
                $x = $data1["coordonnees"][0];
                $y = $data1["coordonnees"][1]; 
                
                echo 'L.marker(['.$x.','.$y.']).addTo(mymap).bindPopup("'."<a href='liste_all_formations.php?etb=".$data1["uai"]."' target='_blank'>".$data1["uo_lib"]."</a>".'");';
            }
            ?>

            
        </script>     
        <div class = "fiche_form">
            <div class = "bloc_formation">
            <h1>Fiche formation</h1>
            <?php 
            echo "<p>Type de disciplines : ".$data["discipline_lib"]."</p>";
            echo "<p>".$data["disciplines_selection"]."</p>";
            echo "<p>Niveau : ".$data["niveau_lib"]."</p>";
            echo "<p>Type de diplôme obtenu : ".$data["diplome_rgp"]."</p>";
            echo "<p> Région : ".$data["reg_ins_lib"]."</p>";
            echo "<p>Département : ".$data["dep_ins_lib"]."</p>";
            echo "<p>Commune : ".$data["com_ins_lib"]."</p>";
            echo "<p> Effectif total : ".$data["effectif"]."</p>";
            echo "<p> Nombre d'hommes : ".$data["hommes"]."</p>";
            echo "<p> Nombre de femmes : ".$data["femmes"]."</p>";?>
            </div>

            
        </div>

       

        <div class = "fiche_etab">
            <?php if (isset($data1)){
            echo "<h1>Fiche établissement</h1>";
            echo "<p>Code UAI: ".$data1["uai"]."</p>";
            echo "<p>Nom de l'établissement : ".$data["etablissement_lib"]."</p>";
            echo "<p>Type d'établissement : ".$data["etablissement_type2"]."</p>";
            echo "<p>Académie : ".$data["aca_etab_lib"]."</p>";
            echo "<p>Adresse : ".$data1["adresse_uai"]."</p>";
            echo "<p>".$data1["uucr_nom"]."</p>";
            echo "<p>".$data1["code_postal_uai"]."</p>";
            echo "<p>N° de téléphone : ".$data1["numero_telephone_uai"]."</p>";
            echo '<a href="'.$data1["url"].'">Lien vers le site de l\'établissement</a>';
            }
            include("footer.php");
            ?>
        </div>
        

</body>
